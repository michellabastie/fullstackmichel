const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: 'checkout', component: () => import('pages/Checkout') },
      { path: 'carrito', component: () => import('pages/Carrito') },
      { path: 'prueba', component: () => import('pages/Prueba') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
