export default {
  username: '',
  rol: '',
  products: [],
  cart: [],
  contador: 0,
  productos: {
    ID1: {
      nombre: 'Weigh Loss Sampler Pack',
      categoria: 'SNACKS',
      costo: 17,
      completed: false
    },
    ID2: {
      nombre: 'Beverage Mix',
      categoria: 'SNACKS',
      costo: 31,
      completed: false
    },
    ID3: {
      nombre: 'Protein Bites',
      categoria: 'SNACKS',
      costo: 41,
      completed: false
    }
  }
}
