export default {
  totalCantidadCarrito: state => {
    let suma = 0
    state.cart.forEach(element => {
      suma += element.quantity
    })
    return suma
  },
  cantidadItems: (state) => {
    return state.cart.length
  },
  productos: (state) => {
    return state.productos
  }
}
