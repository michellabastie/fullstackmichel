import { CARGAR_PRODUCTOS } from './types'

export default {
  loadProducts ({ commit }) {
    // Axios
    fetch('https://api.mockaroo.com/api/82662230?count=1000&key=4585d760')
      .then((result) => {
        return result.json()
      })
      .then((productos) => {
        commit(CARGAR_PRODUCTOS, productos)
      })
      .catch(er => {
        console.log(er)
      })
  }
}
