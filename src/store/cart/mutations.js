import { LOGIN, AGREGAR_PRODUCTO, CARGAR_PRODUCTOS } from './types'

export default {
  [LOGIN]: function (state, data) {
    state.username = data.userName
    state.rol = data.rol
  },
  [AGREGAR_PRODUCTO]: function (state, product) {
    state.cart.push({
      productName: product.nombre,
      quantity: product.cantidad
    })
  },
  [CARGAR_PRODUCTOS]: function (state, products) {
    state.products = products
  }
}
