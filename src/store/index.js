import Vuex from 'vuex'
import Vue from 'vue'

import moduloCart from './cart'
import moduloCheckout from './checkout'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    cart: moduloCart,
    checkout: moduloCheckout
  }
})
